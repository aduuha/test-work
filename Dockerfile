"Перепишите Dockerfile
FROM ubuntu:20.04
COPY ./src /app
RUN apt-get update -y
RUN apt-get install -y nodejs
RUN npm install
ENTRYPOINT ["npm"]
CMD ["run", "prod"]

#переделанный dockerfile
FROM ubuntu:20.04 # базовый образом для сборки Ubuntu 20.04
WORKDIR /app # устанавливает рабочую директорию
RUN apt-get update -y && \ # обновляет список пакетов
RUN apt-get install -y nodejs && \ #устанавливает Node.js
RUN apt-get clean && \ # очищает кэш пакетного менеджера apt-get
RUN rm -rf /var/lib/apt/lists/* # очистка после установки для уменьшения размера 
COPY ./src /app # копирует содержимое директории ./src в директорию /app внутри Docker-образа
COPY package*.json ./ # копирует файлы package.json в текущую директорию внутри Docker-образа
RUN npm install --production # устанавливает зависимости Node.js-приложения
CMD ["npm", "run", "prod"] # задает команду, которая будет выполнена при запуске контейнера, будет запущен скрипт "prod" из файла package.json с помощью команды "npm run"
