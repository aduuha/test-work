#prometheus.service

#Systemd unit, обеспечивает запуск сервера Prometheus,

#поддерживает работу процесса и перезапуска его в случае выхода из строя.

#Предусмотрена возможность перезагрузки конфигурации сервиса без его рестарта.

#sudo vim /etc/systemd/system/prometheus.service

#sudo systemctl daemon-reload

#sudo systemctl start prometheus

#sudo systemctl status prometheus
